After initialization, the system goes immediately into "RUN" mode, commands and menus can be accessed by the buttons.

All button presses should be about 1s in length to register.

Menu:
  UP: Sets the RUN AT pressure (High water mark pressure) [15s timeout after any button press except 'RIGHT (red)']
    UP: Increases the setpoint by 0.01 PSI for every second the button is held
    DOWN: Decreases the setpoint by 0.01 PSI for every second the button is held
    RIGHT: Returns to RUN mode in one second

  DOWN: Sets the STOP AT pressure (Low water mark pressure) [15s timeout after any button press except 'RIGHT (red)']
    UP: Increases the setpoint by 0.01 PSI for every second the button is held
    DOWN: Decreases the setpoint by 0.01 PSI for every second the button is held
    RIGHT: Returns to RUN mode in one second

  LEFT (blue): Immediately sets vacuum to OFF and is only overridden by a press of the RIGHT (red) button, or power reset.

  RIGHT (red): Immediately sets vacuum to ON and runs until button is released.

