/*
 * Based on code from Adafruit for the Adafruit MPRLS Sensor, and Adafruit
 * NeoPixels.
 *
 * https://www.adafruit.com/products/3965
 *
 * Adafruit invests time and resources providing this open source code,
 * please support Adafruit and open-source hardware by purchasing
 * products from Adafruit!
 *
 * Original example code was Written by Limor Fried/Ladyada for Adafruit 
 * Industries.  
 *
 * MIT license, all text here must be included in any redistribution.
*/

#include <Wire.h>
#include "Adafruit_MPRLS.h"
#include <LiquidCrystal_I2C.h>
#include <Adafruit_NeoPixel.h>

Adafruit_MPRLS mpr = Adafruit_MPRLS();

// Pin Settings

const int buttonUpPin = 11;
const int buttonDownPin = 10;
const int buttonLeftPin = 6;
const int buttonRightPin = 5;
// Legacy setting for single button operation
const int buttonPin = 5;

// Output settings
const int relayPin = 8;

// NeoPixel
#define LED_PIN   3
#define LED_COUNT 1
Adafruit_NeoPixel pixel(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
// SET THE PRESSURE HERE (In hPa or PSI, comment the one you didn't use out)

// High point 14.05

// Low point 13.31
// hPa
//const float triggerPressure = 900;
// PSI
float triggerPressure = 13.75;
float lowPoint = 13.32;

// Misc variables
int timer = 0;
int buttonUDLRState = 0;
float pressure = 0;
// Over all button state (only one can be pressed)
int bState = 0;

int buttonState = 0;
int buttonUpState = 0;
int buttonDownState = 0;
int buttonLeftState = 0;
int buttonRightState = 0;
String state;

// Settings for the LCD:
LiquidCrystal_I2C lcd(0x20,16,2);
int Contrast=75;


void setup() {
  pixel.begin();
  pixel.show(); // Initialize all pixels to 'off'
  Serial.begin(115200);
  Serial.println("Vacbed Pressure Control.");
  lcd.init();
  lcd.backlight();
  lcd.home();
  //lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Vacbed control");
  lcd.setCursor(0, 1);
  lcd.print("initializing...");
  delay(2000);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("DO NOT USE");
  lcd.setCursor(0, 1);
  lcd.print("ALONE!!!!!!");
  while (timer <= 5) {
    pixel.setPixelColor(0, 255, 0, 0);
    pixel.show();
    delay(1000);
    pixel.setPixelColor(0, 0, 0, 0);
    pixel.show();
    timer++;
    delay(1000);
  }
  pixel.setBrightness(64);
  pixel.show();
  if (! mpr.begin()) {
    Serial.println("Failed to communicate with MPRLS sensor, check wiring?");
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Sensor Fail");
    lcd.setCursor(1, 0);
    lcd.print("Check Sensor");
    while (1) {
      delay(10);
    }
  }
  Serial.println("Found MPRLS sensor");
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("PSI:");
  lcd.setCursor(0, 1);
  lcd.print("hPa:");
  // initialize the LED pin as an output:
  pinMode(relayPin, OUTPUT);
  pinMode(buttonPin, INPUT);
}


void resetDisplay() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("                ");
  lcd.setCursor(0, 1);
  lcd.print("                ");
  lcd.setCursor(0,0);
  lcd.print("PSI:");
  lcd.setCursor(0, 1);
  lcd.print("hPa:");
}

int readButtonStates() {
  buttonUpState = digitalRead(buttonUpPin);
  buttonDownState = digitalRead(buttonDownPin);
  buttonLeftState = digitalRead(buttonLeftPin);
  buttonRightState = digitalRead(buttonRightPin);
  if (buttonUpState == LOW) {
    buttonUDLRState = 1;
  } else if (buttonDownState == LOW) {
    buttonUDLRState = 2;
  } else if (buttonLeftState == LOW) {
    buttonUDLRState = 3;
  } else if (buttonRightState == LOW) {
    buttonUDLRState = 4;
  } else {
    buttonUDLRState = 0;
  }
  return buttonUDLRState;
}

void runVacuum() {
  digitalWrite(relayPin, HIGH);
  state = "on";
  lcd.setCursor(11,1);
  lcd.print("     ");
  lcd.setCursor(10,0);
  lcd.print("|");
  lcd.setCursor(11,0);
  lcd.print("State");
  lcd.setCursor(10,1);
  lcd.print("|");
  lcd.setCursor(11,1);
  if (bState == 4) {
    // Override
    lcd.print("ORIDE");
    pixel.setPixelColor(0, 0, 100, 100);
  } else {
    lcd.print("RUN");
    pixel.setPixelColor(0, 100, 100, 0);
  }
  pixel.show();
  pressure = updatePressure();
}

void stopVacuum() {
  digitalWrite(relayPin, LOW);
  state = "off";
  lcd.setCursor(11,1);
  lcd.print("     ");
  lcd.setCursor(10,0);
  lcd.print("|");
  lcd.setCursor(11,0);
  lcd.print("State");
  lcd.setCursor(10,1);
  lcd.print("|");
  lcd.print("OFF");
  pixel.setPixelColor(0, 255, 0, 0);
  pixel.show();
  pressure = updatePressure();
}

float updatePressure() {
  float pressure_hPa = mpr.readPressure();
  Serial.print("Pressure (hPa): "); Serial.println(pressure_hPa);
  
  float pressurePsi = pressure_hPa / 68.947572932;
  Serial.print("Pressure (PSI): "); Serial.println(String(pressurePsi));
  lcd.setCursor(4, 0);
  lcd.print(String(pressurePsi));
  lcd.setCursor(4, 1);
  lcd.print(String(pressure_hPa));
  return pressurePsi;
}

void loop() {
   /* we're going to need to read the pressure, and then decide if we need to trigger the relay for the vacuum
   *  once we trigger the vacuum, we want to check the pressure every second until it hits the expected pressure,
   *  once it goes above the preferred pressure, it will close the relay again.
   */
  buttonState = digitalRead(buttonPin);
  buttonUpState = digitalRead(buttonUpPin);
  buttonDownState = digitalRead(buttonDownPin);
  buttonLeftState = digitalRead(buttonLeftPin);
  buttonRightState = digitalRead(buttonRightPin);
  
  pressure = updatePressure();
  if (buttonUpState == LOW) {
    // Draw the Run At Pressure menu
    timer = 0;
    resetDisplay();
    while (timer <= 15) {
      bState = readButtonStates();
      if (bState == 1) {
        // Up Button pressed, reset the counter, and then also increment the up pressure
        triggerPressure = triggerPressure + 0.01;
        timer = 0;
        bState = readButtonStates();
        delay(1000);
      } else if (bState == 2) {
        // Down button pressed, reset the counter, increment the pressure down
        triggerPressure = triggerPressure - 0.01;
        timer = 0;
        bState = readButtonStates();
        delay(1000);
      } else if (bState == 3) {
        // Do Nothing
      } else if (bState == 4) {
        // immediately break and go back to the run menu
        timer = 14;
      }
      lcd.setCursor(0, 0);
      lcd.print("Run at Pressure:");
      lcd.setCursor(0, 1);
      lcd.print("PSI: ");
      lcd.setCursor(6, 1);
      lcd.print(triggerPressure);
      timer++;
      delay(1000);
    }
    resetDisplay();
    
  }
  if (buttonDownState == LOW) {
    // Draw the Stop At Pressure menu
    timer = 0;
    resetDisplay();
    while (timer <= 15) {
      bState = readButtonStates();
      if (bState == 1) {
        // Up Button pressed, reset the counter, and then also increment the up pressure
        lowPoint = lowPoint + 0.01;
        timer = 0;
        bState = readButtonStates();
        delay(1000);
      } else if (bState == 2) {
        // Down button pressed, reset the counter, increment the pressure down
        lowPoint = lowPoint - 0.01;
        timer = 0;
        bState = readButtonStates();
        delay(1000);
      } else if (bState == 3) {
        // Do Nothing
      } else if (bState == 4) {
        // immediately break and go back to the run menu
        timer = 14;
      }
      lcd.setCursor(0, 0);
      lcd.print("Stop Pressure:");
      lcd.setCursor(0, 1);
      lcd.print("PSI: ");
      lcd.setCursor(6, 1);
      lcd.print(lowPoint);
      timer++;
      delay(1000);
    }
    resetDisplay();
  }
  if (buttonLeftState == LOW) {
    bState = readButtonStates();
    // Override and disable the vaccum until "override button is pressed
    while (bState == 3) {
      stopVacuum();
      bState = readButtonStates();
      if (bState != 4) {
        bState = 3;
      }
      delay(1000);
    }
    delay(1000);
  }
  if (buttonRightState == LOW) {
    // Override and run the vacuum
    // Run until the button is released
    bState = readButtonStates();
    while (bState == 4) {
      runVacuum();
      bState = readButtonStates();
      delay(1000);
    }
  }
  
  //if (buttonState == HIGH) {
  if (pressure >= triggerPressure) {
    // Turn on the LED to indicate we're going to flip on the vacuum
    runVacuum();
  } else if (pressure <= lowPoint) {
    stopVacuum();
  }
  delay(1000);

}
