# Vac Bed controller

 
 I AM NOT RESPONSIBLE FOR MISUSE OF THIS PROJECT.  FOR THE LOVE OF ALL THATS GOOD
 AND HOLY DO NOT USE THIS THING ALONE.
 

This project is a "simple" pressure sensor based controller for a vac bed.
It works by reading the pressure inside of the sealed vacbed, and determining
if the vacuum needs to be turned on. (Duh?)

##### Components
* Arduino Uno
* Adafruit i2c MPRLS pressure sensor
* i2c 16x2 LCD Display
* Digital Loggers Inc IoT Relay (https://www.adafruit.com/product/2935)

##### Other things
You may need to adjust the code, specifically around the pressures for your 
environment.

You can set the high water mark, and low water mark pressures once the unit is booted
however, it can only be done in 0.01 PSI increments, if you need more, you'll want to
update the code and burn it to your Arduino.

##### Menu Details
After initialization, the system goes immediately into "RUN" mode, commands and menus 
can be accessed by the buttons.

All button presses should be about 1s in length to register.

**Menu**:

  **UP**: Sets the RUN AT pressure (High water mark pressure) [15s timeout after any 
                                               button press except 'RIGHT (red)']

    UP: Increases the setpoint by 0.01 PSI for every second the button is held
    DOWN: Decreases the setpoint by 0.01 PSI for every second the button is held
    RIGHT: Returns to RUN mode in one second


  **DOWN**: Sets the STOP AT pressure (Low water mark pressure) [15s timeout after any
                                                 button press except 'RIGHT (red)']

    UP: Increases the setpoint by 0.01 PSI for every second the button is held
    DOWN: Decreases the setpoint by 0.01 PSI for every second the button is held
    RIGHT: Returns to RUN mode in one second


  **RIGHT**: Returns to RUN mode in one second


  **LEFT** (blue): Immediately sets vacuum to OFF and is only overridden by a press of the
               RIGHT (red) button, or power reset.


  **RIGHT** (red): Immediately sets vacuum to ON and runs until button is released.